
ActiveAdmin.register Candidate do
  permit_params :first_name

  index do
    column :first_name
  end
 
  action_item :export_candidate_to_pdf, only: :show do 
    link_to 'PDF Export', pdf_export_admin_candidate_path(resource.id, format: :pdf)
  end


  member_action :pdf_export do
  #   respond_to do |format|
  #     format.pdf do 
  #       send_cv_pdf
  #     end
  #   end
  end

  controller do
    self.layout 'cv_layout' 
    
    def pdf_export
      # respond_to do |format|
      #   format.pdf do 
          send_cv_pdf
      #   end
      # end
    end

    def send_cv_pdf
      candidate = Candidate.find(params[:id]) 
      candidate_pdf = CandidatePdf.new(candidate)
      send_file candidate_pdf.to_pdf,
        filename: candidate_pdf.filename,
        type: "application/pdf",
        disposition: "inline"
    end


  end
end