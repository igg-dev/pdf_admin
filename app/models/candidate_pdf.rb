class CandidatePdf
  
  def initialize(candidate)
    @candidate = candidate
    @assets = {}
    ['Elite-logo.png', 'pdf/phone.png', 'pdf/comment.png', 'pdf/map.png', 'pdf/grad.png', 'pdf/case.png', 'pdf/profile.png', 'pdf/lang.png', 'pdf/mouse.png',
      ].each do |img|
      @assets[img] = Base64.strict_encode64(Rails.application.assets[img].to_s)
      
    end
  end

  def to_pdf
      kit = PDFKit.new(as_html)
      cv_pdf = kit.to_file("#{Rails.root}/public/cv.pdf")
  end

  def filename
    "cv_#{candidate.first_name}#{candidate.id}"
  end
  private

  attr_reader :assets
  attr_reader :candidate
  
  def as_html
    x=renderer.render template: "admin/candidates/pdf_export.html", layout: "layouts/cv_layout.html", locals: { candidate: candidate, assets: assets }
  end


  protected

    def renderer
      controller = ActionController::Base.new
      view = ActionView::Base.new(ActionController::Base.view_paths, {}, controller)
    end
end