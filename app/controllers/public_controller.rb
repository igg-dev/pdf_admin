class PublicController < ApplicationController
  def index
    
  end

  def cv
    respond_to do |format|
      format.pdf{ send_cv_pdf}
      if Rails.env.development?
        format.html { as_html }
      end
    end
  end
  
  def cv2
    @candidate = Candidate.first
    respond_to do |format|
      format.html
      format.pdf do
        send_file @candidate.to_pdf,
          filename: @candidate.first_name,
          type: "application/pdf",
          disposition: "inline"
      end
    end
    
  end

  def send_cv_pdf
    kit = PDFKit.new(as_html, root_url: 'http://localhost:3000/')
    cv_pdf = kit.to_file("#{Rails.root}/public/cv.pdf")
    send_file cv_pdf.to_pdf,
      filename: cv_pdf.filename,
      type: "application/pdf",
      disposition: "inline"
  end
      
  private
  
    attr_reader :invoice
  
    def as_html
      render template: "public/cv.html", layout: "cv_layout"#, locals: { invoice: invoice }
    end      
end